import BasicLayout from '@/layouts/basic-layout';

const meta = {
    auth: true
}

const pre = 'statistic_';

export default {
    path: '/admin/statistic',
    name: 'statistic',
    header: 'statistic',
    redirect: {
        name: `${pre}product`
    },
    component: BasicLayout,
    children: [
        {
            path: 'product',
            name: `${pre}product`,
            meta: {
                title: '产品统计'
            },
            component: () => import('@/pages/statistic/product/index')
        },
        {
            path: 'user',
            name: `${pre}user`,
            meta: {
                title: '用户统计'
            },
            component: () => import('@/pages/statistic/user/index')
        },
        {
            path: 'transaction',
            name: `${pre}transaction`,
            meta: {
                title: '交易统计'
            },
            component: () => import('@/pages/statistic/transaction/index')
        }
    ]
};
