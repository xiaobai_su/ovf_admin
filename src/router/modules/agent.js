import BasicLayout from '@/layouts/basic-layout';

const pre = 'agent_';
const meta = {
    auth: true
};
export default {
    path: '/admin/agent',
    name: 'agent',
    header: 'agent',
    redirect: {
        name: `${pre}agentManage`
    },
    meta,
    component: BasicLayout,
    children: [
        {
            path: 'agent_manage/index',
            name: `${pre}agentManage`,
            meta: {
                auth: ['agent-agent-manage'],
                title: '分销员管理'
            },
            component: () => import('@/pages/agent/agentManage')
        },
        {
            path: 'winter_work',
            name: `${pre}winterWork`,
            meta: {
                auth: ['agent-agent-manage'],
                title: '寒假工列表'
            },
            component: () => import('@/pages/agent/winter_work')
        },
        {
            path: 'jiaxiao',
            name: `${pre}jiaxiao`,
            meta: {
                auth: ['agent-agent-manage'],
                title: '驾校报名'
            },
            component: () => import('@/pages/agent/jiaxiao')
        }
    ]
};
