import BasicLayout from '@/layouts/basic-layout';

const pre = 'scenic_';

export default {
    path: '/admin/scenic',
    name: 'scenic',
    header: 'scenic',
    redirect: {
        name: `${pre}scenicList`
    },
    component: BasicLayout,
    children: [
        {
            path: 'scenic_list',
            name: `${pre}scenicList`,
            meta: {
                title: '景点管理',
                auth: ['admin-scenic-list']
            },
            component: () => import('@/pages/scenic/scenic')
        },
        {
            path: 'add_scenic',
            name: `${pre}scenicAdd`,
            meta: {
                auth: ['scenic-add'],
                title: '景点添加'
            },
            component: () => import('@/pages/scenic/scenic/add')
        },
        {
            path: 'scenic_line',
            name: `${pre}scenicLine`,
            meta: {
                title: '线路管理',
                auth: ['admin-scenic-line']
            },
            component: () => import('@/pages/scenic/scenicLine')
        },
        {
            path: 'scenic_product',
            name: `${pre}scenicProduct`,
            meta: {
                title: '景区产品',
                auth: ['admin-scenic-product']
            },
            component: () => import('@/pages/scenic/scenicLine')
        },
    ]
};
