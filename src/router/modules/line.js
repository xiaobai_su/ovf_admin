import BasicLayout from '@/layouts/basic-layout';

const pre = 'line_';

export default {
    path: '/admin/line',
    name: 'line',
    header: 'line',
    redirect: {
        name: `${pre}customBusList`
    },
    component: BasicLayout,
    children: [
        {
            path: 'custom_bus_list',
            name: `${pre}customBusList`,
            meta: {
                title: '线路管理',
                auth: ['admin-line-custom-bus-list']
            },
            component: () => import('@/pages/line/customBusList')
        },
        {
            path: 'add_coustom_bus',
            name: `${pre}coustomBusAdd`,
            meta: {
                auth: ['admin-line-custom-bus-list'],
                title: '线路添加'
            },
            component: () => import('@/pages/line/customBusAdd')
        },
        {
            path: 'line_plan',
            name: `${pre}linePlan`,
            meta: {
                title: '排班管理',
                auth: ['admin-line-plan']
            },
            component: () => import('@/pages/line/linePlan')
        },
    ]
};
