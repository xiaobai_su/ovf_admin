import BasicLayout from '@/layouts/basic-layout';

const pre = 'order_';

export default {
    path: '/admin/order',
    name: 'order',
    header: 'order',
    redirect: {
        name: `${pre}list`
    },
    component: BasicLayout,
    children: [
        {
            path: 'list',
            name: `${pre}list`,
            meta: {
                auth: ['admin-order-storeOrder-index'],
                title: '产品订单'
            },
            component: () => import('@/pages/order/orderList/index')
        },
        {
            path: 'order_bus_list',
            name: `${pre}orderbus`,
            meta: {
                auth: ['admin-order-orderBus-index'],
                title: '车票订单'
            },
            component: () => import('@/pages/order/orderBusList/index')
        },
        {
            path: 'offline',
            name: `${pre}offline`,
            meta: {
                auth: ['admin-order-offline'],
                title: '收银订单'
            },
            component: () => import('@/pages/order/offline/index')
        },
        {
            path: 'invoice/list',
            name: `${pre}invoice`,
            meta: {
                auth: ['admin-order-startOrderInvoice-index'],
                title: '发票管理'
            },
            component: () => import('@/pages/order/invoice/index')
        },
        {
            path: 'queue/list',
            name: `${pre}queue`,
            meta: {
                // auth: ['admin-order-startOrderInvoice-index'],
                title: '任务列表'
            },
            component: () => import('@/pages/order/orderList/handle/queueList')
        }
    ]
};
