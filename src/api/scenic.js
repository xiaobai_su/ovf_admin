import request from '@/plugins/request';


/**
 * @description 景点管理-- 提交 保存景点信息
 */
 export function saveScenicApi (data) {
    return request({
        url: `scenic/save_scenic`,
        method: 'POST',
        data
    });
}



/**
 * @description 景点管理-- 景点列表
 */
 export function scenicLstApi (data) {
    return request({
        url: `scenic/lst`,
        method: 'GET',
        params: data
    });
}

/**
 * @description 景点管理-- 单条景点信息
 */
 export function getScenicDetailApi (id) {
    return request({
        url: `scenic/get_scenic_detail/${id}`,
        method: 'GET',
    });
}


/**
 * @description 景点管理 -- 修改状态
 * @param {Object} param params {Object} 传值参数
 */
 export function setStatusApi (id,status) {
    return request({
        url: `scenic/set_status/${id}/${status}`,
        method: 'PUT'
    });
}

/**
 * @description 景点管理-- 提交 保存景区线路
 */
 export function saveScenicLineApi (data) {
    return request({
        url: `scenic/save_scenic_line`,
        method: 'POST',
        data
    });
}

/**
 * @description 景点管理-- 线路列表
 */
 export function scenicLineLstApi (data) {
    return request({
        url: `scenic/line_lst`,
        method: 'GET',
        params: data
    });
}

/**
 * @description 景点管理-- 线路排班
 */
 export function saveScenicLinePlanApi (data) {
    return request({
        url: `scenic/save_scenic_line_plan`,
        method: 'POST',
        data
    });
}

/**
 * @description 景点管理-- 线路班次列表
 */
 export function getScenicLinePlanApi (data) {
    return request({
        url: `scenic/get_scenic_line_plan`,
        method: 'POST',
        data
    });
}


/**
 * @description 景点管理-- 改变班次状态
 */
 export function changeScenicPlanStatusApi (data) {
    return request({
        url: `scenic/change_scenic_plan_status`,
        method: 'POST',
        data
    });
}
