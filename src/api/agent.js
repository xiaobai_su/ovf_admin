import request from '@/plugins/request';

/**
 * @description 分销 -- 列表
 * @param {Object} param params {Object} 传值参数
 */
export function agentListApi (params) {
    return request({
        url: 'agent/index',
        method: 'get',
        params
    });
}


/**
 * @description 驾校 -- 列表
 * @param {Object} param params {Object} 传值参数
 */
 export function getjiaxiaoList (params) {
    return request({
        url: 'agent/get_jiaxiao_list',
        method: 'get',
        params
    });
}



/**
 * @description 驾校导出 -- 列表
 * @param {Object} param params {Object} 传值参数
 */
 export function exportJiaxiaoList (params) {
    return request({
        url: 'export/jiaxiaoList',
        method: 'get',
        params
    });
}


/**
 * @description 寒假工 -- 列表
 * @param {Object} param params {Object} 传值参数
 */
 export function getWinterWorkList (params) {
    return request({
        url: 'agent/get_winter_work_list',
        method: 'get',
        params
    });
}



/**
 * @description 寒假工导出 -- 列表
 * @param {Object} param params {Object} 传值参数
 */
 export function exportWinterWorkList (params) {
    return request({
        url: 'export/winterWorkList',
        method: 'get',
        params
    });
}


/**
 * @description 分销 -- 表头
 * @param {Object} param params {Object} 传值参数
 */
export function statisticsApi (params) {
    return request({
        url: 'agent/statistics',
        method: 'get',
        params
    });
}

/**
 * @description 分销 -- 推广人,订单列表
 * @param {Object} param params {Object} 传值参数
 * @param {String} param url {String} 请求地址
 */
export function stairListApi (url, params) {
    return request({
        url: url,
        method: 'get',
        params
    });
}

/**
 * @description 分销 -- 公众号推广二维码
 * @param {Object} param params {Object} 传值参数
 */
export function lookCodeApi (params) {
    return request({
        url: 'agent/look_code',
        method: 'get',
        params
    });
}

/**
 * @description 分销 -- 小程序推广二维码
 * @param {Object} param params {Object} 传值参数
 */
export function lookxcxCodeApi (params) {
    return request({
        url: 'agent/look_xcx_code',
        method: 'get',
        params
    });
}

/**
 * @description 分销 -- h5推广二维码
 * @param {Object} param params {Object} 传值参数
 */
export function lookh5CodeApi (params) {
    return request({
        url: 'agent/look_h5_code',
        method: 'get',
        params
    });
}

/**
 * @description 分销 -- 用户推广列表导出
 */
export function userAgentApi (data) {
    return request({
        url: `export/userAgent`,
        method: 'get',
        params: data
    });
}

/**
 * @description 修改上级用户
 * @param {Object} param params {Object} 传值参数
 */
export function agentSpreadApi (data) {
    return request({
        url: 'agent/spread',
        method: 'PUT',
        data
    })
}
