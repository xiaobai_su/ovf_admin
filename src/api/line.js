import request from '@/plugins/request';

/*
 * 获取线路表单头数量；
 * */
export function getLineHeade () {
    return request({
        url: 'line/line/type_header',
        method: 'get'
    });
}


/**
 * @description 线路管理-- 列表
 */
 export function getLines (params) {
    return request({
        url: 'line/line',
        method: 'get',
        params
    });
}


/**
 * @description 线路管理-- 提交 保存定制公交基础信息
 */
 export function saveCustomBusBaseInfoApi (data) {
    return request({
        url: `line/custom_bus_base_info/${data.id}`,
        method: 'POST',
        data
    });
}


/**
 * @description 线路管理-- 提交 保存定制公交运营信息
 */
 export function saveBaseOperationApi (data) {
    return request({
        url: `line/save_base_operation`,
        method: 'POST',
        data
    });
}



/**
 * @description 线路管理-- 提交 保存定制公交线路站点
 */
 export function saveLineStopApi (data) {
    return request({
        url: `line/save_line_stop/${data.id}`,
        method: 'POST',
        data
    });
}


/**
 * @description 线路管理-- 提交 保存线路价格
 */
 export function saveLinePrice (data) {
    return request({
        url: `line/save_line_price`,
        method: 'POST',
        data
    });
}



/**
 * @description 线路管理-- 获取一条线路信息
 */
 export function getCustomBusApi (id) {
    return request({
        url: `line/get_custom_bus/${id}`,
        method: 'get',
    });
}


/**
 * @description 线路管理-- 获取线路route
 */
 export function getStopRoutes (data) {
    return request({
        url: `line/get_stop_routes`,
        method: 'POST',
        data
    });
}



/**
 * @description 线路管理-- 保存班次版本
 */
 export function saveRegularVersion (data) {
    return request({
        url: `line/save_regular_version`,
        method: 'POST',
        data
    });
}


/**
 * @description 线路管理-- 获取班次版本信息
 */
 export function getRegularVersionApi (line_id) {
    return request({
        url: `line/get_regular_version/${line_id}`,
        method: 'get',
    });
}

/**
 * @description 线路管理-- 保存班次版本的班次
 */
 export function saveRegularApi (data) {
    return request({
        url: `line/save_regular`,
        method: 'POST',
        data
    });
}

/**
 * @description 线路管理-- 改变线路状态
 */
 export function changeLineStatusApi (data) {
    return request({
        url: `line/change_line_status`,
        method: 'POST',
        data
    });
}

/**
 * @description 线路管理-- 生成签到码
 */
 export function createSignApi (data) {
    return request({
        url: `line/create_sign`,
        method: 'POST',
        data
    });
}



/**
 * @description 线路管理 -- 线路导出
 */
 export function exportLineListApi (data) {
    return request({
        url: `export/lineList`,
        method: 'get',
        params: data
    });
}

/**
 * @description 线路管理 -- 班次导出
 */
 export function exportLinePlanListApi (data) {
    return request({
        url: `export/linePlanList`,
        method: 'get',
        params: data
    });
}

/**
 * @description 线路管理 -- 班次短信
 */
 export function sendPlanSmsApi (data) {
    return request({
        url: `sms/send_plan_sms`,
        method: 'post',
        data
    });
}



/**
 * @description 线路管理 -- 乘客导出
 */
 export function exportBusPassengerListApi (data) {
    return request({
        url: `export/busPassengerList`,
        method: 'get',
        params: data
    });
}




/**
 * @description 线路管理 -- 线路搜索
 */
 export function searchLineApi (data) {
    return request({
        url: `line/search_line`,
        method: 'get',
        params: data
    });
}


/**
 * @description 排班管理-- 获取排班
 */
 export function getLinePlan (data) {
    return request({
        url: `line/get_line_plan`,
        method: 'POST',
        data
    });
}


/**
 * @description 线路管理-- 批量排班
 */
 export function addLineRegularApi (data) {
    return request({
        url: `line/add_line_regular`,
        method: 'POST',
        data
    });
}

/**
 * @description 线路管理-- 临时排班
 */
 export function addTemporaryPlanApi (data) {
    return request({
        url: `line/add_temporary_plan`,
        method: 'POST',
        data
    });
}



/**
 * @description 线路管理-- 批量修改班次
 */
 export function batchEditPlanApi (data) {
    return request({
        url: `line/batch_edit_plan`,
        method: 'POST',
        data
    });
}
/**
 * @description 线路管理-- 修改班次
 */
 export function changePlanStatusApi (data) {
    return request({
        url: `line/change_plan_status`,
        method: 'POST',
        data
    });
}

